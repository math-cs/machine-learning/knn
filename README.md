# Mines vs. Rocks
**This KNN model distinguishes between sonar signals bounced off a metal
cylinder and those bounced off a roughly cylindrical rock.**

**We do a grid search to find the best performing value of the
parameter k for the KNN model.**
## Data
The data set contains signals obtained from a variety of different
aspect angles for the cylinder and the rock.

Each pattern is a set of 60 numbers in the range 0.0 to 1.0. Each
number represents the energy within a particular frequency band,
integrated over a certain period of time.

The label associated with each record contains the letter "R" if the
object is a rock and "M" if it is a mine (metal cylinder).

Data Source:
https://archive.ics.uci.edu/ml/datasets/connectionist+bench+(sonar,+mines+vs.+rocks)
